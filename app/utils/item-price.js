export default function (item) {
  const unit_price = parseFloat(item.price);
  const count = parseInt(item.order_count, 10);

  let price = unit_price * count;

  // Apply discount rules, if any.
  if (item.discount_rules) {
    for (const rule of item.discount_rules) {
      // Currently, three-for-two is the only supported rule.
      if (rule === 'three-for-two') {
        // Calculate how many times to apply the discount.
        const threes = Math.floor(count / 3);

        if (threes > 0) {
          price = price - (threes * unit_price);
        }
      }
    }
  }

  return price.toFixed(2);
}
