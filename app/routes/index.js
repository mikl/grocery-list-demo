import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    // In a real app, this is where we’d have models and communication
    // with the server. For a demo, a simple JavaScript object will suffice.
    return {
      cart: [
        {
          sku: 'apple',
          label: 'Apple',
          label_plural: 'Apples',
          order_count: 0,
          price: 0.25
        },
        {
          sku: 'orange',
          label: 'Orange',
          label_plural: 'Oranges',
          order_count: 0,
          price: 0.30
        },
        {
          sku: 'bananas',
          label: 'Banana',
          label_plural: 'Bananas',
          order_count: 0,
          price: 0.15
        },
        {
          sku: 'papaya',
          label: 'Papaya',
          label_plural: 'Papayas',
          order_count: 0,
          price: 0.50,
          discount_rules: [
            'three-for-two'
          ]
        },
      ]
    }
  }
});

