import Component from '@ember/component';
import itemPrice from '../utils/item-price'
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['well', 'order-receipt'],

  /**
   * Computed property, getting all non-empty order rows.
   */
  orderLines: computed('cart.@each.order_count', function () {
    const lines = [];

    this.get('cart').forEach(function (item) {
      if (item.order_count >= 1) {
        lines.push(item)
      }
    });

    return lines;
  }),

  total: computed('cart.@each.order_count', function () {
    let total = 0;

    this.get('cart').forEach(function (item) {
      total = total + parseFloat(itemPrice(item))
    });

    return total.toFixed(2);
  })
});
