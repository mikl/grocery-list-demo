import Component from '@ember/component';
import itemPrice from '../utils/item-price'
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['order-receipt-line'],
  classNameBindings: ['line.sku'],

  /**
   * Computed property for label, handles pluralization.
   */
  label: computed('line.{label,label_plural,order_count}', function () {
    const count = parseInt(this.get('line.order_count'), 10);

    if (count > 1) {
      return this.get('line.label_plural')
    }
    else {
      return this.get('line.label')
    }
  }),

  /**
   * Computed price property.
   */
  price: computed('line.{price,order_count}', function () {
    return itemPrice(this.get('line'));
  })
});
