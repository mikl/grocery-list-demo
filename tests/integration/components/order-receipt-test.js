import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('order-receipt', 'Integration | Component | order receipt', {
  integration: true
});

test('it renders nothing with an empty cart', function(assert) {
  this.set('cart', [
    {
      sku: 'potato',
      label: 'Potato',
      label_plural: 'Potatoes',
      order_count: 0,
      price: 0.30
    },
    {
      sku: 'tomato',
      label: 'Tomato',
      label_plural: 'Tomatoes',
      order_count: 0,
      price: 0.15
    },
  ]);

  // Template block usage:
  this.render(hbs`{{order-receipt cart=cart}}`);

  assert.equal(this.$().text().trim(), 'Your cart is empty.');
});

test('it renders with items in the cart', function(assert) {
  this.set('cart', [
    {
      sku: 'potato',
      label: 'Potato',
      label_plural: 'Potatoes',
      order_count: 1,
      price: 0.30
    },
    {
      sku: 'tomato',
      label: 'Tomato',
      label_plural: 'Tomatoes',
      order_count: 20,
      price: 0.15
    },
  ]);


  // Template block usage:
  this.render(hbs`{{order-receipt cart=cart}}`);

  assert.equal(this.$('.potato .name').text().trim(), 'Potato');
  assert.equal(this.$('.potato .price').text().trim(), '$0.30');
  assert.equal(this.$('.tomato .name').text().trim(), 'Tomatoes');
  assert.equal(this.$('.tomato .price').text().trim(), '$3.00');
  assert.equal(this.$('.total .price').text().trim(), '$3.30');
});

test('it renders with discounted items in the cart', function(assert) {
  this.set('cart', [
    {
      sku: 'tomato',
      label: 'Tomato',
      label_plural: 'Tomatoes',
      order_count: 10,
      price: 0.15,
      discount_rules: [
        'three-for-two'
      ]
    },
  ]);

  this.render(hbs`{{order-receipt cart=cart}}`);

  assert.equal(this.$('.tomato .price').text().trim(), '$1.05');

  this.set('cart', [
    {
      sku: 'tomato',
      label: 'Tomato',
      label_plural: 'Tomatoes',
      order_count: 21,
      price: 0.15,
      discount_rules: [
        'three-for-two'
      ]
    },
  ]);

  this.render(hbs`{{order-receipt cart=cart}}`);

  assert.equal(this.$('.tomato .price').text().trim(), '$2.10');
  assert.equal(this.$('.total .price').text().trim(), '$2.10');
});
