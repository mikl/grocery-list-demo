import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('shopping-cart', 'Integration | Component | shopping cart', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{shopping-cart}}`);

  // No cart produces no output.
  assert.equal(this.$().text().trim(), '');

  this.set('cart', [
    {
      sku: 'potato',
      label: 'Potato',
      label_plural: 'Potatoes',
      order_count: 0,
      price: 0.30
    },
    {
      sku: 'tomato',
      label: 'Tomato',
      label_plural: 'Tomatoes',
      order_count: 5,
      price: 0.15
    },
  ]);

  // Render with cart data.
  this.render(hbs`{{shopping-cart cart=cart}}`);

  // Test initial state.
  assert.equal(this.$('#potato').val(), '0');
  assert.equal(this.$('#tomato').val(), '5');

  // Test two-way binding.
  this.set('cart.0.order_count', '1');
  assert.equal(this.$('#potato').val(), '1');
  this.$('#tomato').val(7);
  this.$('#tomato').change();
  assert.equal(this.get('cart.1.order_count'), '7');
});
