import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('order-receipt-line', 'Integration | Component | order receipt line', {
  integration: true
});

test('it renders (plural)', function(assert) {
  this.set('line', {
    sku: 'tomato',
    label: 'Tomato',
    label_plural: 'Tomatoes',
    order_count: 5,
    price: 0.99
  });

  this.render(hbs`{{order-receipt-line line=line}}`);

  assert.equal(this.$('.count').text().trim(), '5');
  assert.equal(this.$('.name').text().trim(), 'Tomatoes');
  assert.equal(this.$('.price').text().trim(), '$4.95');
});

test('it renders (singular)', function(assert) {
  this.set('line', {
    sku: 'tomato',
    label: 'Tomato',
    label_plural: 'Tomatoes',
    order_count: 1,
    price: 0.99
  });

  this.render(hbs`{{order-receipt-line line=line}}`);

  assert.equal(this.$('.count').text().trim(), '1');
  assert.equal(this.$('.name').text().trim(), 'Tomato');
  assert.equal(this.$('.price').text().trim(), '$0.99');
});
